"""
Per eseguire questo codice, prima installare il server redis da 
"https://redis.io/download", avviarlo e poi il client redis 
tramite "pip install redis".

Successivamente lanciare questo file con "python leaderboard.py" 
"""

import redis

def redis_connect() -> redis.client.Redis:
    try:
        client = redis.Redis(host="localhost", port=6379)
        ping = client.ping()
        if ping:
            return client
    except redis.AuthenticationError:
        print("AuthenticationError")

if __name__ == '__main__':
	leaderboard_name = 'leaderboard'
	client = redis_connect()

	while True:
	    opt = input(f"Che operazione si desidera effettuare sul tabellone \'{leaderboard_name}\' ?\n\t1) Vedere il tabellone\n\t2) Modificare il punteggio di una squadra\n\t3) Aggiungere una nuova squadra\n\t4) Eliminare una squadra\n\t0) Uscita\n")

	    try:
	        opt = int(opt)
	    except ValueError:
	        print("Devi inserire un intero!\n")

	    # controllo sulla condizione di uscita
	    if not opt:
	        break
	    elif opt == 1:
	        if not client.zcard(leaderboard_name):
	            print('La leaderboard è vuota!\n')
	        else:
	            print(f'Questi sono i punteggi del tabellone \'{leaderboard_name}\':')
	            i = 1
	            # stampa della classifica
	            for team in client.zrange(leaderboard_name, 0, -1, withscores=True, desc=True):
	                name, score = team
	                print(f"{i}) {name.decode('UTF-8')}: {score} punti\n")
	                i += 1
	    elif opt == 2:
	    	if not client.zcard(leaderboard_name):
	            print('La leaderboard è vuota!\n')
	    	else:	
		        team_edit = input(f"Selezionare la squadra da modificare ({', '.join([name.decode('UTF-8') for name, score in client.zrange(leaderboard_name, 0, -1, withscores=True, desc=True)])}): ")
		    	# modifica della squadra se esiste
		        if bytes(team_edit, 'UTF-8') in client.zrange(leaderboard_name, 0, -1):
		            team_delta_score = int(input('Inserire di quanto modificare il punteggio(negativo per diminuire):'))
		            client.zincrby(leaderboard_name, team_delta_score, team_edit)
		            print("Punteggio modificato correttamente!\n")
		        else:
		            print("La squadra non esiste!\n")
	    elif opt == 3:
	        new_team_name = input(f"Inserire il nome della nuova squadra: ")
	        # aggiunta di una squadra se non esiste
	        if not bytes(new_team_name, 'UTF-8') in client.zrange(leaderboard_name, 0, -1):
	            new_team_score = int(input(f'Inserire il punteggio di {new_team_name}: '))
	            client.zadd(leaderboard_name, {new_team_name: new_team_score})
	            print('Squadra aggiunta correttamente!\n')
	        else:
	            print("La squadra esiste già!\n")
	    elif opt == 4:
	    	if not client.zcard(leaderboard_name):
	            print('La leaderboard è vuota!\n')
	    	else:	
		        rem_team_name = input(f"Inserire il nome della squadra da rimuovere ({', '.join([name.decode('UTF-8') for name, score in client.zrange(leaderboard_name, 0, -1, withscores=True, desc=True)])}): ")
		        # eliminazione di una squadra se non esiste
		        if bytes(rem_team_name, 'UTF-8') in client.zrange(leaderboard_name, 0, -1):
		            client.zrem(leaderboard_name, rem_team_name)
		            print(f"Squadra {rem_team_name} rimossa con successo!\n")
		        else:
		            print("La squadra non esiste!\n")